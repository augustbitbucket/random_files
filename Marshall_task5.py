import datetime
import logging

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

@api.route('/v1/price/<ticker>/<startdateinput>/<enddateinput>')
@api.param('ticker', 'The stock ticker to get a price for')
class Price(Resource):
    def get(self, ticker, startdateinput, enddateinput):
        #start_date = datetime.datetime.now() - datetime.timedelta(days = 7)
        #end_date = datetime.datetime.now() - datetime.timedelta(days = 1)
        today_date = datetime.datetime.now() - datetime.timedelta(days = 1)

        start_date = datetime.date.fromisoformat(startdateinput)
        end_date = datetime.date.fromisoformat(enddateinput)
        print(start_date)
        print(end_date)


        try:
            df_period = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
            #df_end = pdr.get_data_yahoo(ticker, end_date)[['Close']]
            df_today = pdr.get_data_yahoo(ticker, today_date)[['Close']]
        except Exception as ex:
            LOG.error('Error getting data: ' + str(ex))
        
        LOG.error('Recieved price data:')
        #LOG.error(str(df))

        #df['Date'] = df.index

        #return {'ticker': ticker,
        #        'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
        #        'price': df.iloc[-1]['Close']}

        #return str(df_period.iloc[0]["Close"]) + "------" + str(df_period.iloc[-1]["Close"]) + " " + start_date.strftime("%Y-%m-%d") + " " + end_date.strftime("%Y-%m-%d")
        #return "For " + ticker + " Stock" + "<br>" + str(df_period.iloc[0]["Close"]) + "------" + str(df_period.iloc[-1]["Close"]) + " " + start_date.strftime("%Y-%m-%d") + " " + end_date.strftime("%Y-%m-%d")
        return {'Stock Name': ticker,
                'Start Date': start_date.strftime("%Y-%m-%d"),
                'Start Price': str(round(df_period.iloc[0]["Close"], 2)),
                'End Date': end_date.strftime("%Y-%m-%d"),
                'End Price': str(round(df_period.iloc[-1]["Close"], 2)),
                'Return Rate': str((round(round(df_period.iloc[-1]["Close"], 2)/round(df_period.iloc[0]["Close"], 2),4)-1)*100) + "%"
        }


@api.route('/v1/stock')
class Stock(Resource):

    @api.expect(stock)
    def post(self):
        LOG.error('Received payload:')
        LOG.error(api.payload)

        # store this record in a database

        return {'result': 'success',
                'ticker': api.payload['ticker']}


if __name__ == '__main__':
    app.run(debug=True)
