import requests
import time
import datetime
db_length = 0
while True:
    
    rt1 = requests.get('http://localhost:8080/trades')
    #print(len(rt1.json()))
    print(".")
    current_length = len(rt1.json())
    if current_length > db_length:
        print("--------------------------------------------")
        print("Someone just created a new trade in database! At")
        print(datetime.datetime.now())
        print("--------------------------------------------")
        db_length = current_length
    elif current_length < db_length:
        print("--------------------------------------------")
        print("Someone just destroyed a trade in database! At")
        print(datetime.datetime.now())
        print("--------------------------------------------")
        db_length = current_length
    for trade in rt1.json():
        if trade["status"] == "REJECTED":
            print("--------------------------------------------")
            print("This below trade is rejected!")
            print(trade)
            print("--------------------------------------------")
    time.sleep(5)