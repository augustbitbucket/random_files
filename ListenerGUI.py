import tkinter as tk
import tkinter.font as font

import tkinter.scrolledtext as st

import requests
import time
import datetime
import threading

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        #self.create_widgets()

        self.start_button = tk.Button(self)
        self.start_button["text"] = "Start Monitoring"
        self.start_button["font"] = font_for_button
        self.start_button["fg"] = "red"
        self.start_button["command"] = self.run_main_task
        self.start_button.pack(side="top")



        self.expand_button = tk.Button(self, text="Expand History", command=self.expand_or_collapse_function)
        self.expand_button["font"] = font_for_button
        self.expand_button["fg"] = "blue"
        #self.expand_button.pack(side="top")



        self.warning_button = tk.Button(self, text="Service is Normal", command=self.warning_button_function)
        self.warning_button["font"] = font_for_button
        self.warning_button["fg"] = "green"
        self.warning_button.pack(side="top")

        # self.quit = tk.Button(self, text="QUIT", fg="red",
        #                       command=self.master.destroy)
        # self.quit.pack(side="bottom")

        self.warning_msg = tk.Label(self, text="\n")
        self.warning_msg["font"] = font_for_text
        self.warning_msg["fg"] = "red"
        self.warning_msg.pack(side="top")

        self.counting_msg = tk.Label(self, text="\n")
        self.counting_msg["font"] = font_for_text
        self.counting_msg["fg"] = "blue"
        self.counting_msg.pack(side="top")

        self.label = tk.Label(self, text="")
        self.label["font"] = font_for_text
        self.label.pack(side="bottom")

        self.searchbar = tk.Entry(self, text="to search")
        self.searchbar.pack(side="top")

        self.searchbutton = tk.Button(self, text="filter history", command=self.search_button_function)
        self.searchbutton.pack(side="top")

        self.in_history_window = False



        self.db_length = 0
        self.created_list = []
        self.rejected_list = []
        self.filled_list = []

        self.brief_text = ""
        self.full_text = ""

        self.scrolltext = st.ScrolledText(self,width=60,height=6,font=("Times New Roman",15))
        self.scrolltext.insert(tk.INSERT, self.full_text)
        self.scrolltext.pack(side="bottom")

        self.already_expanded = False
        self.under_warning = False

    def search_button_function(self):
        if not self.in_history_window:

            filter_keyword = self.searchbar.get()
            #print(filter_keyword)
            full_test_list = self.full_text.split("\n")
            filtered_text = ""
            for eachline in full_test_list:
                if filter_keyword in eachline:
                    filtered_text += eachline
                    filtered_text += "\n"
            #self.label["text"] = filtered_text
            self.scrolltext.delete(1.0, tk.END)
            self.scrolltext.insert(tk.INSERT, filtered_text)
            self.searchbutton["text"] = "return to all history"
            self.in_history_window = True
        else:
            self.scrolltext.delete(1.0, tk.END)
            self.scrolltext.insert(tk.INSERT, self.full_text)
            self.scrolltext.see(tk.END)
            self.searchbutton["text"] = "filter history"
            self.in_history_window = False


    def warning_button_function(self):
        self.warning_msg["text"] = "\n"
        self.warning_button["text"] = "Service is Normal"
        self.warning_button["fg"] = "green"


    def expand_or_collapse_function(self):
        if not self.already_expanded:
            #self.label["text"] = self.full_text
            self.expand_button["text"] = "Collapse History"

            self.searchbar.pack(side="top")
            self.searchbutton.pack(side="top")

            self.already_expanded = True
        else:
            self.label["text"] = self.brief_text
            self.expand_button["text"] = "Expand History"

            self.searchbar.pack_forget()
            self.searchbutton.pack_forget()

            self.already_expanded = False



    def run_main_task(self):

        #self.scrolltext.delete(1.0, tk.END)
        #self.scrolltext.insert(tk.INSERT, self.full_text)


        threading.Timer(1.0, self.run_main_task).start()





        rt1 = requests.get('http://localhost:8080/trade')
        # print(len(rt1.json()))
        # print("...")
        current_length = len(rt1.json())
        # if current_length > self.db_length:
        #     print("--------------------------------------------")
        #     print("Someone just created a new trade in database! At")
        #     print(datetime.datetime.now())
        #     print("--------------------------------------------")
        #
        #
        #
        #     self.label["text"] = "Someone just created a new trade in database! At"
        #
        #     self.db_length = current_length
        # elif current_length < self.db_length:
        #     print("--------------------------------------------")
        #     print("Someone just destroyed a trade in database! At")
        #     print(datetime.datetime.now())
        #     print("--------------------------------------------")
        #     self.db_length = current_length

        time_string = str(datetime.datetime.now().hour) + ":" + str(datetime.datetime.now().minute) + ":" + str(datetime.datetime.now().second)
        self.start_button["text"] = "Monitoring...   " + time_string
        self.start_button["fg"] = "black"

        count_created = 0
        count_filled = 0
        count_rejected = 0

        have_new_activity = False
        text_this_second = ""

        for trade in rt1.json():
            if (trade["state"] == "PROCESSING"):
                count_created += 1
            if (trade["state"] == "CREATED"):
                count_created += 1
                if (trade["id"]["timestamp"] not in self.created_list):
                    have_new_activity = True
                    self.created_list.append(trade["id"]["timestamp"])
                    print("--------------------------------------------")
                    print("This below trade is just created")
                    print(trade)
                    print("--------------------------------------------")
                    self.brief_text = time_string + "     CREATED: " + trade["stockTicker"] + "-" + str(trade["id"]["timestamp"]) + "\n"
                    self.full_text += self.brief_text
                    text_this_second += self.brief_text
                    # if self.already_expanded:
                    #     self.label["text"] = self.full_text
                    #     self.scrolltext.insert(tk.INSERT, self.brief_text)
                    # else:
                    #     self.label["text"] = self.brief_text
                    # self.expand_button["text"] = "Expand History"
                    # self.already_expanded = False
                #count_created += 1

            if (trade["state"] == "REJECTED"):
                count_rejected += 1
                if (trade["id"]["timestamp"]not in self.rejected_list):
                    have_new_activity = True
                    self.rejected_list.append(trade["id"]["timestamp"])
                    print("--------------------------------------------")
                    print("This below trade is rejected!")
                    print(trade)
                    print("--------------------------------------------")
                    self.brief_text = time_string + "     REJECTED: " + trade["stockTicker"] + "-" + str(trade["id"]["timestamp"]) + "\n"
                    self.full_text += self.brief_text
                    text_this_second += self.brief_text
                    # if self.already_expanded:
                    #     self.label["text"] = self.full_text
                    #     self.scrolltext.insert(tk.INSERT, self.brief_text)
                    # else:
                    #     self.label["text"] = self.brief_text
                    self.warning_msg["text"] += "Warning! " + trade["stockTicker"] + "-" + str(trade["id"]["timestamp"]) + " is rejected!\n"
                    self.warning_button["text"] = "Service has Warnings, press here to ignore all warnings"
                    self.warning_button["fg"] = "red"
                    #count_rejected += 1

            if (trade["state"] == "FILLED"):
                count_filled += 1
                if (trade["id"]["timestamp"] not in self.filled_list):
                    have_new_activity = True
                    self.filled_list.append(trade["id"]["timestamp"])
                    print("--------------------------------------------")
                    print("This below trade is filled!")
                    print(trade)
                    print("--------------------------------------------")
                    self.brief_text = time_string + "     FILLED: " + trade["stockTicker"] + "-" + str(trade["id"]["timestamp"]) + "\n"
                    self.full_text += self.brief_text
                    text_this_second += self.brief_text
                    # if self.already_expanded:
                    #     self.label["text"] = self.full_text
                    #     self.scrolltext.insert(tk.INSERT, self.brief_text)
                    # else:
                    #     self.label["text"] = self.brief_text
                    #count_filled += 1

            # if (trade["state"] == "CREATED"):
            #     count_created += 1
            # if (trade["state"] == "FILLED"):
            #     count_filled += 1
            # if (trade["state"] == "REJECTED"):
            #     count_rejected += 1

        self.counting_msg["text"] = "Waiting: " + str(count_created) + "   Filled: " + str(count_filled) + "   Rejected: " + str(count_rejected) + "\n" + "\n"
        #self.scrolltext.insert(tk.INSERT, self.brief_text)
        if not self.in_history_window:
            if have_new_activity:
                self.scrolltext.insert(tk.INSERT, text_this_second)
                self.scrolltext.see(tk.END)
        self.label["text"] = self.brief_text


root = tk.Tk()
font_for_button = font.Font(size=20)
font_for_text = font.Font(size=15)
app = Application(master=root)
app.mainloop()

